/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package instrumento.entidades;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Steven Villalobos
 */
public class Calibracion {
    int numero;
    Date fecha;
    Instrumento instrumento;
    List<Medida> medidas;

    public Calibracion() {
        this.numero = 0;
        this.fecha = null;
        this.instrumento = null;
        this.medidas = null;
    }
    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getNumero() {
        return numero;
    }
    public Instrumento getInstrumento() {
        return instrumento;
    }

    public void setInstrumento(Instrumento instrumento) {
        this.instrumento = instrumento;
    }

    public Calibracion(int numero, Date fecha, Instrumento instrumento, List<Medida> medidas) {
        this.numero = numero;
        this.fecha = fecha;
        this.instrumento = instrumento;
        this.medidas = medidas;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public void setMedidas(List<Medida> medidas) {
        this.medidas = medidas;
    }

    public Date getFecha() {
        return fecha;
    }

    public List<Medida> getMedidas() {
        return medidas;
    }
    @Override
    public String toString() {
        return Integer.toString(medidas.size());
    }
}
