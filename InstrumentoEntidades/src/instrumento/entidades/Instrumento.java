/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package instrumento.entidades;

/**
 *
 * @author Steven Villalobos
 */
public class Instrumento {
    String serie;
    String descripcion;
    TipoInstrumento tipoInstrumento;
    int min;
    int max;
    int tol;

    public Instrumento(String serie, String descripcion, TipoInstrumento tipoInstrumento, int min, int max, int tol) {
        this.serie = serie;
        this.descripcion = descripcion;
        this.tipoInstrumento = tipoInstrumento;
        this.min = min;
        this.max = max;
        this.tol = tol;
    }
    public Instrumento() {
        this.serie = "";
        this.descripcion = "";
        this.tipoInstrumento = null;
        this.min = 0;
        this.max = 0;
        this.tol = 0;  

    }
    public void setSerie(String serie) {
        this.serie = serie;
    }

    public void setTipoInstrumento(TipoInstrumento tipoInstrumento) {
        this.tipoInstrumento = tipoInstrumento;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public void setTol(int tol) {
        this.tol = tol;
    }

    public String getSerie() {
        return serie;
    }

    public TipoInstrumento getTipoInstrumento() {
        return tipoInstrumento;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    public int getTol() {
        return tol;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public String getDescripcion() {
        return descripcion;
    }

}
