/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package instrumento.entidades;

/**
 *
 * @author Steven Villalobos
 */
public class Medida {
    int medida;
    int calibracion;
    int referencia;
    int lectura;

    public Medida() {
        this.calibracion = 0;
        this.medida = 0;
        this.referencia = 0;
        this.lectura = 0;
    }

    public void setCalibracion(int calibracion) {
        this.calibracion = calibracion;
    }

    public int getCalibracion() {
        return calibracion;
    }

    public Medida(int calibracion,int medida, int referencia, int lectura) {
        this.calibracion = calibracion;
        this.medida = medida;
        this.referencia = referencia;
        this.lectura = lectura;
    }

    public void setMedida(int Medida) {
        this.medida = Medida;
    }

    public void setReferencia(int referencia) {
        this.referencia = referencia;
    }

    public void setLectura(int lectura) {
        this.lectura = lectura;
    }

    public int getMedida() {
        return medida;
    }

    public int getReferencia() {
        return referencia;
    }

    public int getLectura() {
        return lectura;
    }

}
