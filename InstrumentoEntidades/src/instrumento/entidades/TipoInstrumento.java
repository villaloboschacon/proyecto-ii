/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package instrumento.entidades;

import java.util.Objects;

/**
 *
 * @author Steven Villalobos
 */
public class TipoInstrumento {
    private String codigo;
    private String nombre;
    private String unidad;    

    public TipoInstrumento() {
        this.codigo = "";
        this.nombre = "";
        this.unidad = "";
    }

    public TipoInstrumento(String codigo, String nombre, String unidad, Instrumento tipoInstumento) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.unidad = unidad;

    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public String getUnidad() {
        return unidad;
    }
    public String toString() {
        return nombre;
    }
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.codigo);
        return hash;
    }
     public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TipoInstrumento other = (TipoInstrumento) obj;
        if (!Objects.equals(this.codigo, other.codigo)) {
            return false;
        }
        return true;
    }
}
