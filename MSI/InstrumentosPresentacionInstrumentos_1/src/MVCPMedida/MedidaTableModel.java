/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVCPMedida;

import instrumento.entidades.Medida;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Steven Villalobos
 */
public class MedidaTableModel extends AbstractTableModel{
    List<Medida> rows;
    int[] cols;
    public  MedidaTableModel(int[] cols, List<Medida> rows){
        this.cols=cols;
        this.rows=rows;
        initColNames();
    }
     public int getColumnCount() {
        return cols.length;
    }

    public List<Medida> getRows() {
        return rows;
    }

    public String getColumnName(int col){
        return colNames[cols[col]];
    } 
    
    public int getRowCount() {
        return rows.size();
    }
    public boolean isCellEditable(int rowIndex, int columnIndex) 
    {
        return true;
    }
    public Object getValueAt(int row, int col) {
        Medida medidas = rows.get(row);
        switch (cols[col]){
            case REFERENCIA: return medidas.getReferencia();
            case LECTURA: return medidas.getLectura();            
            default: return "";
        }
    } 
    public void setValueAt(Object value, int row, int col)
    {
        Medida medida = rows.get(row);
        String x = "";
        int test = 0;
        int y = 0;
        if(value.getClass() == Integer.class)
        {
            test = 2;
            y = (int)value;
        }
        else
        {
           x = (String)value;
           test = 1;
        }

        switch(cols[col])
        {
            case REFERENCIA: 
                if(test == 1)
                    medida.setReferencia(Integer.valueOf(x));
                else if(test == 2)
                    medida.setReferencia(y);
            break;
            case LECTURA: 
                if(test == 1)
                    medida.setLectura(Integer.valueOf(x));
                else if(test == 2)
                    medida.setLectura(y);
            break;
            
        }
        rows.set(row, medida);
    }

    private void initColNames(){
        colNames[REFERENCIA]= "REFERENCIA";
        colNames[LECTURA]= "LECTURA";   
    }
    public Medida getRowAt(int row) {
        return rows.get(row);
    }
    
    public static final int REFERENCIA = 0;
    public static final int LECTURA = 1;  
    String[] colNames = new String[2];  
}
