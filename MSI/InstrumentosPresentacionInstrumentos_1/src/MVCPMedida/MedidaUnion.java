/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVCPMedida;

/**
 *
 * @author Steven Villalobos
 */
public class MedidaUnion {
    MedidaModel model;
    MedidaController controller;
    
    public void setController(MedidaController controller){
        this.controller=controller;
    }
    public void setModel(MedidaModel model){
        this.model=model;
    }

    public MedidaModel getModel() {
        return model;
    }

    public MedidaController getController() {
        return controller;
    }
}
