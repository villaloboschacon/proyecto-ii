/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVCPMedida;

import instrumento.logica.Model;


/**
 *
 * @author Steven Villalobos
 */
public class MedidaController {
    Model domainModel;
    MedidaModel model;
    MedidaUnion medidaUnion;
    public MedidaController(MedidaModel model, Model domainModel, MedidaUnion medidaUnion) 
      {
          model.init();
          this.domainModel= domainModel;
          this.medidaUnion = medidaUnion;
          this.model = model;
          medidaUnion.setController(this);
          medidaUnion.setModel(model);
      }

    }

