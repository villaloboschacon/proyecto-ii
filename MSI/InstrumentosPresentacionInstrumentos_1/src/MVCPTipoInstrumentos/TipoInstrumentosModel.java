/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVCPTipoInstrumentos;

import MVCPTipoInstrumento.TipoInstrumentoTableModel;
import instrumento.entidades.TipoInstrumento;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Observer;

/**
 *
 * @author Steven Villalobos
 */
public class TipoInstrumentosModel extends java.util.Observable{
 
    TipoInstrumento filter; 
    TipoInstrumentoTableModel tipoinstrumentos;
    HashMap<String,String> errores;
    String mensaje;

    public TipoInstrumentosModel() {
    }
    public void setTipoInstrumento(List<TipoInstrumento> tipoinstrumentos){
        int[] cols={TipoInstrumentoTableModel.CODIGO,TipoInstrumentoTableModel.NOMBRE,TipoInstrumentoTableModel.UNIDAD};
        this.tipoinstrumentos =new TipoInstrumentoTableModel(tipoinstrumentos, cols);  
        setChanged();
        notifyObservers();        
    }
    
    public void init()
    { 
        filter = new TipoInstrumento();
        List<TipoInstrumento> rows = new ArrayList<TipoInstrumento>();
        this.setTipoInstrumento(rows);
        clearErrors();
    }
    
    public TipoInstrumento getFilter() {
        return filter;
    }
    
    public void setFilter(TipoInstrumento filter) {
        this.filter = filter;
    }
    
     public TipoInstrumentoTableModel getTipoInstrumentos() {
        return tipoinstrumentos;
    }
    
     @Override
    public void addObserver(Observer o) {
        super.addObserver(o);
        setChanged();
        notifyObservers();
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public HashMap<String, String> getErrores() {
        return errores;
    }

    public void setErrores(HashMap<String, String> errores) {
        this.errores = errores;
    }
    
    public void clearErrors(){
        setErrores(new HashMap<String,String>());
        setMensaje(""); 
    }
}
