/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVCPTipoInstrumentos;

import MVCPTipoInstrumento.TipoInstrumentoModel;
import instrumento.entidades.TipoInstrumento;
import instrumento.logica.Model;
import instrumentos.presentacion.Application;
import java.util.List;


/**
 *
 * @author Steven Villalobos
 */
public class TipoInstrumentosController {
    Model domainModel;
    TipoInstrumentosModel model;
    TipoInstrumentosView view;
    
    public TipoInstrumentosController(Model domainModel, TipoInstrumentosModel model, TipoInstrumentosView view) {
        model.init();
        this.domainModel = domainModel;
        this.model = model;
        this.view = view;
        view.setController(this);
        view.setModel(model);
    }
     public void buscar(){
        model.getFilter().setCodigo(view.buscarFld.getText());
        model.clearErrors();
        List<TipoInstrumento> rows = domainModel.searchTipoInstrumento(model.getFilter());
        if (rows.isEmpty())
        {
            model.getErrores().put("buscarFld", "Ningun registro coincide");
            model.setMensaje("NINGUN TIPO DE INSTRUMENTO COINCIDE");
        }
        model.setTipoInstrumento(rows);
    }

    public void preAgregar()
    {
        TipoInstrumentoModel tipoinstrumentoModel = Application.TIPO_INSTRUMENTO_VIEW.getModel();
        tipoinstrumentoModel.clearErrors();
        tipoinstrumentoModel.setModo(Application.MODO_AGREGAR);
        tipoinstrumentoModel.setCurrent(new TipoInstrumento());
        Application.TIPO_INSTRUMENTO_VIEW.setVisible(true);
        Application.TIPO_INSTRUMENTO_VIEW.toFront();
    }
    
    public void editar(int row)
    {
        TipoInstrumentoModel tipoinstrumentoModel = Application.TIPO_INSTRUMENTO_VIEW.getModel();
        tipoinstrumentoModel.clearErrors();
        TipoInstrumento seleccionada = model.getTipoInstrumentos().getRowAt(row); 
        tipoinstrumentoModel.setModo(Application.MODO_EDITAR);
        tipoinstrumentoModel.setCurrent(seleccionada);
        Application.TIPO_INSTRUMENTO_VIEW.setVisible(true);
        Application.TIPO_INSTRUMENTO_VIEW.toFront();
    }

    public void borrar(int row){
        TipoInstrumento seleccionada = model.getTipoInstrumentos().getRowAt(row); 
        try 
        {
            domainModel.deleteTipoInstrumento(seleccionada);
        } 
        catch (Exception ex) { }
        this.buscar();
    }
    
    public void salir(){
        domainModel.close();
    }
}
