/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVCPCalibraciones;

import MVCPCalibracion.CalibracionTableModel;
import MVCPMedida.MedidaTableModel;
import MVCPMedidas.MedidasModel;
import instrumento.entidades.Calibracion;
import instrumento.entidades.Medida;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Steven Villalobos
 */
public class CalibracionesModel extends java.util.Observable{
    Calibracion calibracionFilter;
    CalibracionTableModel calibraciones;
    HashMap<String,String> errores;
    String mensaje;
    
    Medida medidaFilter;
    MedidaTableModel medidas;

    public Medida getMedidaFilter() {
        return medidaFilter;
    }
    
    public void init()
    { 
        medidaFilter = new Medida();
        calibracionFilter = new Calibracion();
        List<Medida> rowsm = new ArrayList<Medida>();
        List<Calibracion> rows = new ArrayList<Calibracion>();
        this.setMedidas(rowsm);
        this.setCalibraciones(rows);
        clearErrors();
    }
    
    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public HashMap<String, String> getErrores() {
        return errores;
    }

    public void setErrores(HashMap<String, String> errores) {
        this.errores = errores;
    }
    
    public void clearErrors(){
        setErrores(new HashMap<String,String>());
        setMensaje("");
        
    }
    public Calibracion getCalibracionFilter() {
        return calibracionFilter;
    }

    public void setFilterCalibracion(Calibracion current) {
        this.calibracionFilter = current;
        setChanged();
        notifyObservers();        
    }
    public void setFilterMedida(Medida current) {
        this.medidaFilter = current;
        setChanged();
        notifyObservers();        
    }
    public void setMedidas(List<Medida> medidas){
        int[] cols={MedidaTableModel.LECTURA,MedidaTableModel.REFERENCIA};
        this.medidas =new MedidaTableModel(cols,medidas);  
        setChanged();
        notifyObservers();        
    }
    public void setCalibraciones(List<Calibracion> calibraciones){
        int[] cols={CalibracionTableModel.FECHA,CalibracionTableModel.MEDICICIONES};
        this.calibraciones =new CalibracionTableModel(cols,calibraciones);  
        setChanged();
        notifyObservers();        
    }
    public MedidaTableModel getMedidas()
    {
        return medidas;
    }
    public CalibracionTableModel getCalibraciones()
    {
        return calibraciones;
    }

    @Override
    public void addObserver(java.util.Observer o) {
        super.addObserver(o);
        setChanged();
        notifyObservers();
    }
}
