/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVCPCalibraciones;

import MVCPCalibracion.CalibracionModel;
import static MVCPMedida.MedidaTableModel.REFERENCIA;
import MVCPMedidas.MedidasController;
import MVCPMedidas.MedidasModel;
import instrumento.entidades.Calibracion;
import instrumento.entidades.Medida;
import instrumento.logica.Model;
import instrumentos.presentacion.Application;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Steven Villalobos
 */
public class CalibracionesController {
    Model domainModel;
    CalibracionesView view;
    CalibracionesModel model;
    
    MedidasModel medidasModel;
    MedidasController medidasController;
    public CalibracionesController(MedidasController medidasController,Model domainModel, CalibracionesModel model, MedidasModel medidasModel,CalibracionesView view) 
    {
        medidasModel.init();
        model.init();
        this.domainModel = domainModel;
        this.model = model;
        this.medidasModel = medidasModel;
        this.view = view;
        this.medidasController = medidasController;        
        view.setModelMedida(medidasModel);
        view.setControllerMedida(medidasController);
        view.setController(this);
        view.setModel(model);

    }
    public void preAgregar()
    {
        CalibracionModel calibracionModel = Application.CALIBRACION_UNION.getModel();
        calibracionModel.clearErrors();
        calibracionModel.setModo(Application.MODO_AGREGAR);
        calibracionModel.setCurrent(new Calibracion());
    }
     public void buscar(){
        int row = Application.INSTRUMENTOS_VIEW.getRow();
        model.getCalibracionFilter().setInstrumento(Application.INSTRUMENTOS_VIEW.getModel().getInstrumentos().getRowAt(row));
        model.clearErrors();
        List<Calibracion> rows = new ArrayList<Calibracion>();
        try 
        {
            rows = domainModel.getCalibracion(model.getCalibracionFilter().getInstrumento().getSerie());
        } 
        catch (Exception ex) {}
        
        if (rows.isEmpty())
        {
            model.getErrores().put("calibracionesTable", "No hay registros");
            model.setMensaje("El INSTRUMENTO NO POSEE CALIBRACIONES");
        }
        List<Medida> medida = new ArrayList<Medida>();
        for(Calibracion c: rows)
        {
            medida = c.getMedidas();
        }
        model.setCalibraciones(rows);
    }     
     public void buscarMedidas(){
        model.clearErrors();
        List<Medida> rows = new ArrayList<Medida>();
        try 
        {
            rows = domainModel.getMedida(model.getCalibracionFilter().getNumero());
        } 
        catch (Exception ex) {}
        if (rows.isEmpty())
        {
            medidasModel.getErrores().put("buscarFld", "Ningun registro coincide");
            medidasModel.setMensaje("NINGUNA MEDIDA COINCIDE");
        }
        model.setMedidas(rows);
        view.setModel(model);
    }

    public void guardar()
    {
        CalibracionModel calibracionModel = Application.CALIBRACION_UNION.getModel();
        CalibracionesModel calibracionesModel = Application.CALIBRACIONES_VIEW.getModel();
        calibracionesModel.clearErrors();
        Calibracion calibracion = new Calibracion();
        calibracion.setFecha(Application.CALIBRACION_UNION.getModel().getDate());
        calibracion.setInstrumento(Application.INSTRUMENTOS_VIEW.getModel().getInstrumentos().getRowAt(Application.INSTRUMENTOS_VIEW.getRow()));
        calibracion.setMedidas(model.getMedidas().getRows());
        int x = 1;           
        for(Medida m: calibracion.getMedidas())
        {
            m.setMedida(x);
            x++;
        }

        List<Calibracion> calibraciones;
        if (calibracionModel.getErrores().isEmpty()){
            try{
                switch(calibracionModel.getModo()){
                    case Application.MODO_AGREGAR: 
                        domainModel.addCalibracion(calibracion);                      
                        calibracionModel.setMensaje("MEDIDA AGREGADA");
                        calibracionModel.setCurrent(new Calibracion());                        
                        calibraciones = domainModel.searchCalibracion(calibracionesModel.getCalibracionFilter());
                        calibracionesModel.setCalibraciones(calibraciones);                       
                        break;
//                    case Application.MODO_EDITAR:
//                        domainModel.updateCalibracion(calibracion);
//                        calibracionModel.setMensaje("MEDIDA MODIFICADADA");
//                        calibracionModel.setCurrent(calibracion);
//                        calibraciones = domainModel.searchCalibracion(calibracionesModel.getCalibracionFilter());
//                        calibracionesModel.setCalibraciones(calibraciones);
//                        view.setVisible(false);
//                        break;
                }
            }
            catch(Exception e){
                calibracionModel.getErrores().put("id", "MEDIDA YA EXISTE");
                calibracionModel.setMensaje("MEDIDA YA EXISTE");
                calibracionModel.setCurrent(calibracion);
            }
        }
        else{
            calibracionModel.setMensaje("ESPACIOS VACIOS.");
            calibracionModel.setCurrent(calibracion);
        }
    }
    public void borrar(int row){
        try 
        {
            domainModel.deleteCalibracion(model.getCalibraciones().getRowAt(row));
        } 
        catch (Exception ex) { }
        this.buscar();
    }
    
    public void salir(){
        domainModel.close();
    }
}
