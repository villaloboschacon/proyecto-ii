/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVCPTipoInstrumento;

import instrumento.entidades.TipoInstrumento;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Steven Villalobos
 */
public class TipoInstrumentoTableModel extends AbstractTableModel{
    List<TipoInstrumento> rows;
    int[] cols;    
    
    public TipoInstrumentoTableModel(List<TipoInstrumento> rows, int[] cols) {
        this.rows = rows;
        this.cols = cols;
        initColNames();
    }
    private void initColNames()
        {
            colNames[CODIGO]= "CODIGO";
            colNames[NOMBRE]= "NOMBRE";
            colNames[UNIDAD]= "UNIDAD";
        }
    public TipoInstrumento getRowAt(int row) 
    {
        return rows.get(row);
    }

    public int getColumnCount() 
    {
        return cols.length;
    }
    public String getColumnName(int col)
    {
        return colNames[cols[col]];
    }
    public int getRowCount() 
    {
        return rows.size();
    }
    
    public static final int CODIGO=0;
    public static final int NOMBRE=1;
    public static final int UNIDAD=2;
    
    String[] colNames = new String[3];
            
    public Object getValueAt(int row, int col) 
    {
        TipoInstrumento ti = rows.get(row);
        switch (cols[col])
        {
            case CODIGO: return ti.getCodigo();
            case NOMBRE: return ti.getNombre();
            case UNIDAD: return ti.getUnidad();
            default: return "";
        }
    }           
        
}
