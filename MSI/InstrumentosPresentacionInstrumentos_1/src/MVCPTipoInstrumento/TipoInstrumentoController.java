/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVCPTipoInstrumento;

import MVCPTipoInstrumentos.TipoInstrumentosModel;
import instrumento.entidades.TipoInstrumento;
import instrumento.logica.Model;
import instrumentos.presentacion.Application;
import java.util.List;
/**
 *
 * @author jsanchez
 */
public class TipoInstrumentoController
{
    Model domainModel;
    TipoInstrumentoView view;
    TipoInstrumentoModel model;

    public TipoInstrumentoController(Model domainModel, TipoInstrumentoView view, TipoInstrumentoModel model) {
        model.init();
        this.domainModel = domainModel;
        this.view = view;
        this.model = model;
        view.setController(this);
        view.setModel(model);
    }
     public void guardar(){
        TipoInstrumentosModel tipoInstrumentosModel = Application.TIPO_INSTRUMENTOS_VIEW.getModel();

        TipoInstrumento nueva = new TipoInstrumento();
        model.clearErrors();
        
        nueva.setCodigo(view.codigoFld.getText());
        if (view.codigoFld.getText().length()==0){
            model.getErrores().put("Codigo", "Codigo requerido");
        }
        nueva.setNombre(view.nombreFld.getText());
        if (view.nombreFld.getText().length()==0){
            model.getErrores().put("Nombre", "Nombre requerido");
        }    
        nueva.setUnidad(view.unidadFld.getText());
        if (view.unidadFld.getText().length()==0){
            model.getErrores().put("Unidad", "Unidad requerido");
        }   
        
        List<TipoInstrumento> tipoInstrumento;
        if (model.getErrores().isEmpty()){
            try{
                switch(model.getModo()){
                    case Application.MODO_AGREGAR:
                        domainModel.addTipoInstrumento(nueva);
                        model.setMensaje("TIPO NSTRUMENTO AGREGADO");
                        model.setCurrent(new TipoInstrumento());                        
                        tipoInstrumento = domainModel.searchTipoInstrumento(tipoInstrumentosModel.getFilter());                        
                        tipoInstrumentosModel.setTipoInstrumento(tipoInstrumento); 
                        Application.TIPO_INSTRUMENTOS_VIEW.setModel(tipoInstrumentosModel);
                        view.setVisible(false);
                        break;
                    case Application.MODO_EDITAR:
                        domainModel.updateTipoInstrumento(nueva);
                        model.setMensaje("TIPO INSTRUMENTO MODIFICADADA");
                        model.setCurrent(nueva);
                        tipoInstrumento = domainModel.searchTipoInstrumento(tipoInstrumentosModel.getFilter());
                        tipoInstrumentosModel.setTipoInstrumento(tipoInstrumento);
                        view.setVisible(false);
                        break;
                }
            }
            catch(Exception e){
                model.getErrores().put("codigo", "TIPO INSTRUMENTO YA EXISTE");
                model.setMensaje("TIPO INSTRUMENTO YA EXISTE");
                model.setCurrent(nueva);
            }
        }
        else{
            model.setMensaje("ESPACIOS VACIOS.");
            model.setCurrent(nueva);
        }
    }
}

