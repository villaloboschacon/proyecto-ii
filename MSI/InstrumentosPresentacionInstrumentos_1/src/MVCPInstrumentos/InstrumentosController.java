/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVCPInstrumentos;

import instrumento.entidades.Instrumento;
import instrumento.logica.Model;
import instrumentos.presentacion.Application;
import java.util.List;

/**
 *
 * @author Steven Villalobos
 */
public class InstrumentosController {
    Model domainModel;
    InstrumentosModel model;
    InstrumentosView view;

    public InstrumentosController(Model domainModel, InstrumentosModel model, InstrumentosView view) {
        model.init();
        this.domainModel = domainModel;
        this.model = model;
        this.view = view;
        view.setController(this);
        view.setModel(model);
    }
     public void buscar(){
        model.getFilter().setSerie(view.busFld.getText());
        model.clearErrors();
        List<Instrumento> rows = domainModel.searchInstrumento(model.getFilter());
        if (rows.isEmpty())
        {
            model.getErrores().put("serieFld", "Ningun registro coincide");
            model.setMensaje("NINGUN REGISTRO COINCIDE");
        }
        model.setInstrumentos(rows);
    }

    public void preAgregar()
    {       
        Application.INSTRUMENTO_VIEW.getModel().clearErrors();
        Application.INSTRUMENTO_VIEW.getModel().setModo(Application.MODO_AGREGAR);
        Application.INSTRUMENTO_VIEW.getModel().setCurrent(new Instrumento());
        Application.INSTRUMENTO_VIEW.setVisible(true);
        Application.INSTRUMENTO_VIEW.toFront();
    }
    
    public void editar(int row)
    {
        Application.INSTRUMENTO_VIEW.getModel().clearErrors();
        Instrumento seleccionada = model.getInstrumentos().getRowAt(row); 
        Application.INSTRUMENTO_VIEW.getModel().setModo(Application.MODO_EDITAR);
        Application.INSTRUMENTO_VIEW.getModel().setCurrent(seleccionada);
        Application.INSTRUMENTO_VIEW.setVisible(true);
        Application.INSTRUMENTO_VIEW.toFront();
    }

    public void borrar(int row)
    {
        try 
        {
            domainModel.deleteInstrumento(model.getInstrumentos().getRowAt(row));
        } 
        catch (Exception ex) { }
        this.buscar();
    }
    
    public void salir(){
        domainModel.close();
    }
    
    
}
