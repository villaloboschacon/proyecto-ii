/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVCPMedidas;

import MVCPMedida.MedidaModel;
import instrumento.entidades.Medida;
import instrumento.logica.Model;
import instrumentos.presentacion.Application;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Steven Villalobos
 */
public class MedidasController {
    Model domainModel;
    MedidasModel model;
    MedidasUnion medidasUnion;
    public MedidasController(Model domainModel, MedidasModel model, MedidasUnion medidasUnion) {
        model.init();
        this.medidasUnion = medidasUnion;
        this.domainModel = domainModel;
        this.model = model;
        medidasUnion.setController(this);
        medidasUnion.setModel(model);
    }
    public void preAgregar(){
        MedidaModel medidaModel = Application.MEDIDA_UNION.getModel();
        medidaModel.clearErrors();
        medidaModel.setModo(Application.MODO_AGREGAR);
        medidaModel.setCurrent(new Medida());
    }
    
    public void editar(int row){
        MedidaModel medidaModel = Application.MEDIDA_UNION.getModel();
        medidaModel.clearErrors();
        Medida seleccionada = model.getMedidas().getRowAt(row); 
        medidaModel.setModo(Application.MODO_EDITAR);
        medidaModel.setCurrent(seleccionada);
    }

    
    public void salir(){
        domainModel.close();
         }
    }


