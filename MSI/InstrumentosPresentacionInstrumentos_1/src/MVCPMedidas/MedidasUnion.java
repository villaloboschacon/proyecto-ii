/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVCPMedidas;

/**
 *
 * @author Steven Villalobos
 */
public class MedidasUnion {
    MedidasModel model;
    MedidasController controller;

    public void setController(MedidasController controller){
        this.controller=controller;
    }
    public void setModel(MedidasModel model){
        this.model=model;
    }

    public MedidasModel getModel() {
        return model;
    }

    public MedidasController getController() {
        return controller;
    }
    
}
