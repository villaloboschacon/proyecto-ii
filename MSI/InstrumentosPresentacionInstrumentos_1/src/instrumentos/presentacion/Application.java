/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package instrumentos.presentacion;

import MVCPCalibracion.CalibracionController;
import MVCPCalibracion.CalibracionModel;
import MVCPCalibracion.CalibracionUnion;
import MVCPCalibraciones.CalibracionesController;
import MVCPCalibraciones.CalibracionesModel;
import MVCPInstrumento.InstrumentoController;
import MVCPInstrumento.InstrumentoModel;
import MVCPInstrumentos.InstrumentosController;
import MVCPInstrumentos.InstrumentosModel;
import MVCPMedida.MedidaController;
import MVCPMedida.MedidaModel;
import MVCPMedida.MedidaUnion;
import MVCPMedidas.MedidasController;
import MVCPMedidas.MedidasModel;
import MVCPMedidas.MedidasUnion;
import MVCPTipoInstrumento.TipoInstrumentoController;
import MVCPTipoInstrumento.TipoInstrumentoModel;
import MVCPTipoInstrumentos.TipoInstrumentosController;
import MVCPTipoInstrumentos.TipoInstrumentosModel;
import instrumento.logica.Model;
import MVCPCalibraciones.CalibracionesView;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import MVCPInstrumento.InstrumentoView;
import MVCPInstrumentos.InstrumentosView;
import MVCPTipoInstrumentos.TipoInstrumentosView;
import MVCPTipoInstrumento.TipoInstrumentoView;
/**
 *
 * @author Steven Villalobos
 */
public class Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Model domainModel = Model.instance();        
        MedidaModel medidaModel = new MedidaModel();
        MedidasModel medidasModel = new MedidasModel();
        
        InstrumentosModel instrumentosModel = new InstrumentosModel();
        InstrumentoModel instrumentoModel = new InstrumentoModel();
        
        InstrumentosView instrumentosView= new InstrumentosView();
        InstrumentoView instrumentoView= new InstrumentoView();
        
        CalibracionModel calibracionModel = new CalibracionModel();
        
        CalibracionesModel calibracionesModel = new CalibracionesModel();
        CalibracionesView calibracionesView = new CalibracionesView();
        
        CalibracionUnion calibracionUnion = new CalibracionUnion();
        CALIBRACION_UNION = calibracionUnion;
        
        MedidasUnion medidasUnion = new MedidasUnion();
        MedidaUnion medidaUnion = new MedidaUnion();
        MEDIDAS_UNION = medidasUnion;
        MEDIDA_UNION = medidaUnion;
        MedidaController medidaController = new MedidaController(medidaModel,domainModel, medidaUnion);
        MedidasController medidasController = new MedidasController(domainModel,medidasModel, medidasUnion);

        CalibracionesController calibracionesController = new CalibracionesController(medidasController,
                domainModel,calibracionesModel,medidasModel,calibracionesView);  

        CalibracionController calibracionController = new CalibracionController(domainModel,calibracionUnion,calibracionModel);
               
        CALIBRACIONES_VIEW = calibracionesView;
       
        
        InstrumentosController instrumentosController = new InstrumentosController(domainModel,instrumentosModel,instrumentosView);
        InstrumentoController instrumentoController = new InstrumentoController(instrumentoView,instrumentoModel,domainModel);
        INSTRUMENTO_VIEW = instrumentoView ;
        INSTRUMENTOS_VIEW = instrumentosView;  
       
        
        TipoInstrumentoModel  tipoInstrumentomodel = new TipoInstrumentoModel();
        TipoInstrumentoView tipoInstrumentoView= new TipoInstrumentoView();
        TipoInstrumentoController  tipoInstrumentoController = new TipoInstrumentoController(domainModel,tipoInstrumentoView,tipoInstrumentomodel);
        TIPO_INSTRUMENTO_VIEW = tipoInstrumentoView;
        

        TipoInstrumentosModel  tipoInstrumentosmodel = new TipoInstrumentosModel();
        TipoInstrumentosView tipoInstrumentosView= new TipoInstrumentosView();
        TipoInstrumentosController  tipoInstrumentosController = new TipoInstrumentosController(domainModel, tipoInstrumentosmodel,tipoInstrumentosView);
        TIPO_INSTRUMENTOS_VIEW = tipoInstrumentosView;
        
        AppView appView= new AppView(calibracionesView,tipoInstrumentosView,tipoInstrumentoView,instrumentoView,instrumentosView);
        appView.setVisible(true);

    }
    public static InstrumentoView INSTRUMENTO_VIEW;
    public static InstrumentosView INSTRUMENTOS_VIEW;    
       
    public static TipoInstrumentoView TIPO_INSTRUMENTO_VIEW; 
    public static TipoInstrumentosView TIPO_INSTRUMENTOS_VIEW; 
    
    public static CalibracionesView CALIBRACIONES_VIEW;
    
    public static CalibracionUnion CALIBRACION_UNION;
    
    public static MedidasUnion MEDIDAS_UNION;
    public static MedidaUnion MEDIDA_UNION;
       
    public static  final int  MODO_AGREGAR=0;
    public static final int MODO_EDITAR=1;
    
    public static Border BORDER_ERROR = BorderFactory.createLineBorder(Color.red);
    public static Border BORDER_NOBORDER = BorderFactory.createLineBorder(Color.red);

}
