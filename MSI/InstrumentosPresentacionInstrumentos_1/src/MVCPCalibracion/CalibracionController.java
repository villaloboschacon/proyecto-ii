/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVCPCalibracion;

import instrumento.logica.Model;;

/**
 *
 * @author Steven Villalobos
 */
public class CalibracionController {
    Model domainModel;
    CalibracionUnion union;
    CalibracionModel model;

        public CalibracionController(Model domainModel, CalibracionUnion union, CalibracionModel model) 
        {
            model.init();
            this.domainModel= domainModel;
            this.union = union;
            this.model = model;
            union.setController(this);
            union.setModel(model);
        }

    }

