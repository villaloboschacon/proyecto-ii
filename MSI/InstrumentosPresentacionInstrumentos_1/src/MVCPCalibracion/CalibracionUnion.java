/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVCPCalibracion;

import java.util.Observable;

/**
 *
 * @author Steven Villalobos
 */
public class CalibracionUnion implements java.util.Observer{
    CalibracionController controller;
    CalibracionModel model;
    
    public void setController(CalibracionController controller){
        this.controller=controller;
    }
    public void setModel(CalibracionModel model){
        this.model=model;
        model.addObserver(this);
    }

    public CalibracionModel getModel() {
        return model;
    }

    @Override
    public void update(Observable o, Object arg) {
        
    }
    
}
