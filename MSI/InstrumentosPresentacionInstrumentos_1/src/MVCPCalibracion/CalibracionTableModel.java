/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVCPCalibracion;

import instrumento.entidades.Calibracion;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Steven Villalobos
 */
public class CalibracionTableModel extends AbstractTableModel{
    List<Calibracion> rows;
    int[] cols;
   
    public  CalibracionTableModel(int[] cols, List<Calibracion> rows){
        this.cols=cols;
        this.rows=rows;
        initColNames();
    }

    public List<Calibracion> getRows() {
        return rows;
    }
        public int getColumnCount() {
        return cols.length;
    }
    public String getColumnName(int col){
        return colNames[cols[col]];
    } 
    
    public int getRowCount() {
        return rows.size();
    }
    
    public Object getValueAt(int row, int col) {
        Calibracion calibracion = rows.get(row);
        switch (cols[col]){
            case FECHA: return calibracion.getFecha();
            case MEDICICIONES: return calibracion.getMedidas().size();            
            default: return "";
        }
    } 
    private void initColNames(){
        colNames[FECHA]= "FECHA";
        colNames[MEDICICIONES]= "MEDICICIONES";   
    }
    public Calibracion getRowAt(int row) {
        return rows.get(row);
    }
    public static final int FECHA = 0;
    public static final int MEDICICIONES = 1;  
    String[] colNames = new String[2]; 
}
