/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVCPCalibracion;

import instrumento.entidades.Calibracion;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;

/**
 *
 * @author Steven Villalobos
 */
public class CalibracionModel extends java.util.Observable{
    Calibracion current;
    HashMap<String,String> errores;
    String mensaje;
    int modo;
    
    public void init()
    {
        setCurrent(new Calibracion());
        clearErrors();
    }

     public int getModo() {
        return modo;
    }

    public void setModo(int modo) {
        this.modo = modo;
    }
    
    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public HashMap<String, String> getErrores() {
        return errores;
    }

    public void setErrores(HashMap<String, String> errores) {
        this.errores = errores;
    }
    
    public void clearErrors(){
        setErrores(new HashMap<String,String>());
        setMensaje("");
        
    }
    public Calibracion getCurrent() {
        return current;
    }

    public void setCurrent(Calibracion current) {
        this.current = current;
        setChanged();
        notifyObservers();        
    }
    public Date getDate()
    {
        java.util.Date d = new java.util.Date(); 
        SimpleDateFormat plantilla = new SimpleDateFormat("dd/MM/yyyy");
        String tiempo = plantilla.format(d);
        java.sql.Date date = new java.sql.Date(d.getTime());
        return date;
    }
    @Override
    public void addObserver(java.util.Observer o) {
        super.addObserver(o);
        setChanged();
        notifyObservers();
    }
}
