/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVCPInstrumento;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import instrumento.entidades.Instrumento;

/**
 *
 * @author jsanchez
 */
public class InstrumentoTableModel extends AbstractTableModel{
    List<Instrumento> rows;
    int[] cols;

    public  InstrumentoTableModel(int[] cols, List<Instrumento> rows){
        this.cols=cols;
        this.rows=rows;
        initColNames();
    }

    public int getColumnCount() {
        return cols.length;
    }

    public String getColumnName(int col){
        return colNames[cols[col]];
    } 
    
    public int getRowCount() {
        return rows.size();
    }

    public Object getValueAt(int row, int col) {
        Instrumento instrumento = rows.get(row);
        switch (cols[col]){
            case SERIE: return instrumento.getSerie();            
            case TIPO: return instrumento.getTipoInstrumento();
            case DESCRIPCION: return instrumento.getDescripcion();
            case MINIMO: return instrumento.getMin();
            case MAXIMO: return instrumento.getMax();
            case TOLERANCIA: return instrumento.getTol();            
            default: return "";
        }
    }    
    private void initColNames(){
        colNames[SERIE]= "SERIE";
        colNames[TIPO]= "TIPO";
        colNames[DESCRIPCION]= "DESCRIPCION";
        colNames[MINIMO]= "MINIMO";
        colNames[MAXIMO]= "MAXIMO";
        colNames[TOLERANCIA]= "TOLERANCIA";     
    }
    public Instrumento getRowAt(int row) {
        return rows.get(row);
    }
    
    public static final int SERIE =0;
    public static final int TIPO=1;
    public static final int DESCRIPCION=2;
    public static final int MINIMO=3;
    public static final int MAXIMO=4;
    public static final int TOLERANCIA=5;    
    String[] colNames = new String[6];  
            
}
