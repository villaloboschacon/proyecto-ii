 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVCPInstrumento;

import MVCPInstrumentos.InstrumentosModel;
import instrumento.entidades.Instrumento;
import instrumento.entidades.TipoInstrumento;
import instrumento.logica.Model;
import instrumentos.presentacion.Application;
import java.util.List;

/**
 *
 * @author Steven Villalobos
 */
public class InstrumentoController {
    Model domainModel;
    InstrumentoView view;
    InstrumentoModel model;
    
    public InstrumentoController(InstrumentoView view, InstrumentoModel model, Model domainModel) 
    {
        model.init(domainModel.getTipoInstrumentos().toArray(new TipoInstrumento[0]));
        this.domainModel= domainModel;
        this.view = view;
        this.model = model;
        view.setController(this);
        view.setModel(model);
    }

    public Model getDomainModel() {
        return domainModel;
    }
    

     public void guardar(){
        InstrumentosModel instrumentosModel = Application.INSTRUMENTOS_VIEW.getModel();

        Instrumento nueva = new Instrumento();
        model.clearErrors();
        
        nueva.setSerie(view.serieFld.getText());
        if (view.serieFld.getText().length()==0){
            model.getErrores().put("Serie", "Serie requerido");
        }
        nueva.setDescripcion(view.descripcionFld.getText());
        if (view.descripcionFld.getText().length()==0){
            model.getErrores().put("Descripcion", "Descripcion requerido");
        }        
        nueva.setMin(Integer.parseInt(view.minFld.getText()));
        if (view.minFld.getText().length()==0){
            model.getErrores().put("Minimo", "Minimo requerido");
        }
        nueva.setMax(Integer.parseInt(view.maxFld.getText()));
        if (view.maxFld.getText().length()==0){
            model.getErrores().put("Maximo", "Maximo requerido");
        }
        nueva.setTol(Integer.parseInt(view.tolFld.getText()));
        if (view.tolFld.getText().length()==0){
            model.getErrores().put("Tolerancia", "Tolerancia requerida");
        }     
        
        nueva.setTipoInstrumento((TipoInstrumento) view.tipoCmbBox.getSelectedItem());
        
        List<Instrumento> instrumento;
        if (model.getErrores().isEmpty()){
            try{
                switch(model.getModo()){
                    case Application.MODO_AGREGAR:
                        domainModel.addInstrumento(nueva);
                        model.setMensaje("INSTRUMENTO AGREGADO");
                        model.setCurrent(new Instrumento());                        
                        instrumento = domainModel.searchInstrumento(instrumentosModel.getFilter());
                        instrumentosModel.setInstrumentos(instrumento);   
                        view.setVisible(false);
                        break;
                    case Application.MODO_EDITAR:
                        domainModel.updateInstrumento(nueva);
                        model.setMensaje("INSTRUMENTO MODIFICADO");
                        model.setCurrent(nueva);
                        instrumento = domainModel.searchInstrumento(instrumentosModel.getFilter());
                        instrumentosModel.setInstrumentos(instrumento);
                        view.setVisible(false);
                        break;
                }
            }
            catch(Exception e){
                model.getErrores().put("id", "INSTRUMENTO YA EXISTE");
                model.setMensaje("INSTRUMENTO YA EXISTE");
                model.setCurrent(nueva);
            }
        }
        else{
            model.setMensaje("ESPACIOS INCOMPLETOS.");
            model.setCurrent(nueva);
        }
    }
     
     public void refreshTipoInstrumento(){
//         model.init(domainModel.getTipoInstrumentos().toArray(new TipoInstrumento[0]));
     }
}
