/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVCPInstrumento;

import instrumento.entidades.Instrumento;
import instrumento.entidades.TipoInstrumento;
import java.util.HashMap;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author Steven Villalobos
 */
public class InstrumentoModel extends java.util.Observable{
 
    Instrumento current;
    ComboBoxModel<TipoInstrumento> tipoinstrumentos;
    HashMap<String,String> errores;
    String mensaje;
    int modo;
    
    public void init(TipoInstrumento[] tipoinstrumentos){
        setTipoInstrumentos(tipoinstrumentos);
        setCurrent(new Instrumento());
        clearErrors();
    }

    public int getModo() {
        return modo;
    }

    public void setModo(int modo) {
        this.modo = modo;
    }
    
    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public HashMap<String, String> getErrores() {
        return errores;
    }

    public void setErrores(HashMap<String, String> errores) {
        this.errores = errores;
    }
    
    public void clearErrors(){
        setErrores(new HashMap<String,String>());
        setMensaje("");
        
    }
    public Instrumento getCurrent() {
        return current;
    }

    public void setCurrent(Instrumento current) {
        this.current = current;
        setChanged();
        notifyObservers();        
    }

    public ComboBoxModel<TipoInstrumento> getTipoInstrumentos() {
        return tipoinstrumentos;
    }

    public void setTipoInstrumentos(TipoInstrumento[] tipoinstrumentos) {
        this.tipoinstrumentos = new DefaultComboBoxModel(tipoinstrumentos);
        setChanged();
        notifyObservers();        
    }

    @Override
    public void addObserver(java.util.Observer o) {
        super.addObserver(o);
        setChanged();
        notifyObservers();
    }
    
}
