    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package instrumento.datos;

import instrumento.entidades.Calibracion;
import instrumento.entidades.TipoInstrumento;
import instrumento.entidades.Instrumento;
import instrumento.entidades.Medida;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

/**
 *
 * @author Steven Villalobos
 */
public class Dao {
        InstrumentoDB db;    

    public Dao()
    {
        db = new InstrumentoDB();
    }
    
    public Instrumento InstrumentoGet(String serie) throws Exception{
        String sql="select * from Instrumento i inner join TipoInstrumento t on i.tipo = t.codigo where i.tipo = '%s'";
        sql = String.format(sql,serie);
        ResultSet rs =  db.executeQuery(sql);
        if (rs.next()) {
            return instrumento(rs);
        }
        else{
            throw new Exception ("Instrumento no Existe");
        }
    }
    public List<Calibracion> CalibracionGet(String serie) throws Exception{
        String sql="select * from Instrumento i inner join Calibracion c on i.serie = c.instrumento where i.serie = '%s'";
        sql = String.format(sql,serie);
        ResultSet rs =  db.executeQuery(sql);
        List<Calibracion> estados=new ArrayList<Calibracion>();
        while (rs.next()) 
        {
            estados.add(calibracion(rs));
        }
        return estados;
    }
    public List<Medida> MedidaGet(int serie) throws Exception{
        String sql="select * from Calibracion c inner join Medida m on c.numero = m.calibracion where c.numero = %d";
        sql = String.format(sql,serie);
        ResultSet rs =  db.executeQuery(sql);
        List<Medida> estados=new ArrayList<Medida>();
        while (rs.next()) 
        {
            estados.add(medidas(rs));
        }
        return estados;
    }
    public List<Calibracion> CalibracionGetAll()
    {
        List<Calibracion> estados=new ArrayList<Calibracion>();
        try 
        {
            String sql="select * from Calibracion";
            ResultSet rs =  db.executeQuery(sql);
            while (rs.next()) 
            {
                estados.add(calibracion(rs));
            }
        } 
        catch (SQLException ex) { }
        return estados;
    }
        
    public Collection<TipoInstrumento> TipoInstrumentoGetAll(){
        Vector<TipoInstrumento> estados=new Vector<TipoInstrumento>();
        try {
            String sql="select * from TipoInstrumento";
            ResultSet rs =  db.executeQuery(sql);
            while (rs.next()) {
                estados.add(tipoInstrumento(rs));
            }
        } catch (SQLException ex) { }
        return estados;        
    }


    public void InstrumentoDelete(Instrumento i) throws Exception{
        String sql="delete from Instrumento where serie = '%s'";
        sql = String.format(sql,i.getSerie());
        Calibracion c = new Calibracion();
        c.setInstrumento(i);
        if(!CalibracionSearch(c).isEmpty())
        {
            Calibracion c2 = CalibracionSearch(c).get(0);
            CalibracionDelete(c2);
        }
        int count=db.executeUpdate(sql);
        if (count==0){
            throw new Exception("Instrumento no existe");
        }
    }
    public void TipoInstrumentoDelete(TipoInstrumento i) throws Exception{
        String sql="delete from TipoInstrumento where codigo='%s'";
        sql = String.format(sql,i.getCodigo());
        int count=db.executeUpdate(sql);
        if (count==0){
            throw new Exception("Tipo Instrumento no existe");
        }
    }
    public void CalibracionDelete(Calibracion i) throws Exception{
        String sql="delete from Medida where calibracion = %d";
        sql = String.format(sql,i.getNumero());
        int count=db.executeUpdate(sql);
        if (count==0)
        {
            throw new Exception("Calibracion no existe");
        }
        sql="delete from Calibracion where numero = %d";
        sql = String.format(sql,i.getNumero(), i.getNumero());
        count = db.executeUpdate(sql);
        if (count==0)
        {
            throw new Exception("Calibracion no existe");
        }
    }

    public void InstrumentoAdd(Instrumento i) throws Exception{
        String sql="INSERT INTO Instrumento (serie,tipo,descripcion,minimo,maximo,tolerancia) VALUES "+
                "('%s','%s','%s',%d,%d,%d)";
        sql=String.format(sql,i.getSerie(),i.getTipoInstrumento().getCodigo(),i.getDescripcion(),i.getMin(),i.getMax(),i.getTol());
        int count=db.executeUpdate(sql);
        if (count==0){
            throw new Exception("Instrumento ya existe");
        }
    }
    public void TipoInstrumentoAdd(TipoInstrumento i) throws Exception{
        String sql="insert into TipoInstrumento (codigo,nombre,unidad)values "+
                "('%s','%s','%s')";
        sql=String.format(sql,i.getCodigo(),i.getNombre(),i.getUnidad());
        int count=db.executeUpdate(sql);
        if (count==0)
        {
            throw new Exception("Tipo Instrumento ya existe");
        }
    }
    public void CalibracionAdd(Calibracion i) throws Exception{
//        String sql="INSERT INTO Calibracion (instrumento,fecha,mediciones,numero) VALUES "+
//                "('%s','%s',%d,%d)";
//        sql=String.format(sql,i.getInstrumento().getSerie(),i.getFecha().toString(),i.getMedidas().size(),i.getNumero());
//       
         String sql="INSERT INTO Calibracion (instrumento,fecha,mediciones) VALUES "+
                "('%s','%s',%d)";
        sql=String.format(sql,i.getInstrumento().getSerie(),i.getFecha().toString(),i.getMedidas().size());
        int numero = db.executeUpdateWithKeys(sql);
//        int count=db.executeUpdate(sql);
        //No se utiliza el numero porque con el UpdateWithKeys por que siempre devuelve -1

        if (numero==0)
        {
            throw new Exception("Calibracion ya existe");
        }
        else if(numero == -1)
        {
            throw new Exception("Calibracion ya existe");
        }
        for(Medida c: i.getMedidas())
        {
            c.setCalibracion(numero);
            MedidasAdd(c);
        }
        
    }
    public void MedidasAdd(Medida i) throws Exception{
        String sql="insert into Medida (calibracion,medida,referencia,lectura)values "+
                "(%d,%d,%d,%d)";
        sql=String.format(sql,i.getCalibracion(),i.getMedida(),i.getReferencia(),i.getLectura());
        int count=db.executeUpdate(sql);
        if (count==0)
        {
            throw new Exception("Calibracion ya existe");
        }
    }

    public void InstrumentoUpdate(Instrumento i) throws Exception{
        String sql="update Instrumento set tipo='%s',descripcion='%s',minimo='%s',maximo='%s',tolerancia='%s'"+
                "where serie='%s'";
        sql=String.format(sql,i.getTipoInstrumento().getCodigo(),i.getDescripcion(),i.getMin(),i.getMax(),i.getTol(),i.getSerie());        
        int count=db.executeUpdate(sql);
        if (count==0){
            throw new Exception("Instrumento no existe");
        }
    }
    public void TipoInstrumentoUpdate(TipoInstrumento i) throws Exception{
        String sql="update TipoInstrumento set nombre='%s',unidad='%s'"+
                "where codigo='%s'";
               sql=String.format(sql,i.getNombre(),i.getUnidad(),i.getCodigo());    
        int count=db.executeUpdate(sql);
        if (count==0){
            throw new Exception("Tipo Instrumento no existe");
        }
    }
    public void CalibracionUpdate(Calibracion i) throws Exception{
        String sql="update Calibracion set instrumento = '%s',fecha = '%s', mediciones = %d where numero = %d";
        sql=String.format(sql,i.getInstrumento().getSerie(),i.getFecha().toString(),i.getMedidas().size(),i.getNumero());        
        int count=db.executeUpdate(sql);
        if (count==0){
            throw new Exception("Calibracion no existe");
        }
        for(Medida c: i.getMedidas())
        {
            MedidasUpdate(c);
        }
    }
    public void MedidasUpdate(Medida i) throws Exception{
        String sql="update Medida set referencia = %d ,lectura = %d where calibracion = %d and medida = %d";
        sql=String.format(sql,i.getReferencia(),i.getLectura(),i.getCalibracion(), i.getMedida());        
        int count=db.executeUpdate(sql);
        if (count==0)
        {
            throw new Exception("Medidas no existe");
        }
    }
    public List<Instrumento> InstrumentoSearch(Instrumento filtro){
        List<Instrumento> resultado = new ArrayList<Instrumento>();
        try {
            String sql="select * from "+
                    "Instrumento i inner join TipoInstrumento t on i.tipo=t.codigo "+
                    "where i.serie like '%%%s%%'";
            sql=String.format(sql,filtro.getSerie());
            ResultSet rs =  db.executeQuery(sql);
            while (rs.next()) {
                resultado.add(instrumento(rs));
            }
        } catch (SQLException ex) { }
        return resultado;
    }
    
    public List<TipoInstrumento> TipoInstrumentoSearch(TipoInstrumento filtro){
        List<TipoInstrumento> resultado = new ArrayList<TipoInstrumento>();
        try {
            String sql="select * from "+
                    "TipoInstrumento "+
                    "where codigo like '%%%s%%'";
            sql=String.format(sql,filtro.getCodigo());
            ResultSet rs =  db.executeQuery(sql);
            while (rs.next()) {
                resultado.add(tipoInstrumento(rs));
            }
        } catch (SQLException ex) { }
        return resultado;
    }
        public List<Calibracion> CalibracionSearch(Calibracion filtro){
        List<Calibracion> resultado = new ArrayList<Calibracion>();
        try {
            String sql="select * from "+
                    "Calibracion"+
                    " where instrumento = '%s'";
            sql=String.format(sql,filtro.getInstrumento().getSerie());
            ResultSet rs =  db.executeQuery(sql);
            while (rs.next()) {
                resultado.add(calibracion(rs));
            }
        } catch (SQLException ex) { }
        return resultado;
    }
    public List<Medida> MedidasSearch(Calibracion filtro){
        List<Calibracion> resultado = new ArrayList<Calibracion>();
        List<Medida> res = new ArrayList<Medida>();
        Calibracion c = new Calibracion();
        int numero = 0;
        try 
        {
            String sql="select * from Calibracion where instrumento = '%s'";
            sql=String.format(sql,filtro.getInstrumento().getSerie());   
            ResultSet rs =  db.executeQuery(sql);
                while (rs.next()) 
                {
                    resultado.add(calibracion(rs));
                }
                numero = filtro.getNumero();
        } 
        catch (SQLException ex)
        {
        
        }
        try
        {
            String sql="select * from Medida where calibracion = %d";
            sql=String.format(sql,numero); 
            ResultSet rs =  db.executeQuery(sql);
            while (rs.next()) 
            {
                res.add(medidas(rs));
            }
        }
        catch(SQLException exs)
        {
             
        }
        return res;
    }
    
    private Instrumento instrumento(ResultSet rs){
        try {
            Instrumento i = new Instrumento();
            i.setSerie(rs.getString("serie"));
            i.setTipoInstrumento(tipoInstrumento(rs));
            i.setDescripcion(rs.getString("descripcion"));
            i.setMin(rs.getInt("minimo"));
            i.setMax(rs.getInt("maximo"));
            i.setTol(rs.getInt("tolerancia"));
            return i;
        } catch (SQLException ex) {
            return null;
        }
    }
    private TipoInstrumento tipoInstrumento(ResultSet rs){
        try {
            TipoInstrumento ec= new TipoInstrumento();
            ec.setCodigo(rs.getString("codigo"));
            ec.setNombre(rs.getString("nombre"));
            ec.setUnidad(rs.getString("unidad"));
            return ec;
        } catch (SQLException ex) {
            return null;
        }
    }
    private Calibracion calibracion(ResultSet rs){
        try {
            Calibracion c = new Calibracion();
            c.setNumero(rs.getInt("numero"));
            c.setInstrumento(instrumento(rs));
            c.setFecha(rs.getDate("fecha"));
            c.setMedidas(mediciones(rs, c.getNumero()));
            return c;
        } catch (SQLException ex) {
            return null;
        }
    }
    private List<Medida> mediciones(ResultSet rs,int numero)
    {       
            List<Medida> c = new ArrayList<Medida>();
            try {
                Medida m = new Medida();
                String sql="select * from "+
                    "Medida "+
                    "where calibracion = '%s'";
                sql=String.format(sql,numero);
                rs =  db.executeQuery(sql);
                while (rs.next())
                {
                    c.add(medidas(rs));
                }            
                return c;
            }
            catch (SQLException ex) {}
            return c;
        }
     private Medida medidas(ResultSet rs)
    {        
        try {
            Medida x= new Medida();
            x.setMedida(rs.getInt("medida"));
            x.setCalibracion(rs.getInt("calibracion"));
            x.setLectura(rs.getInt("lectura"));
            x.setReferencia(rs.getInt("referencia"));
            return x;
        } catch (SQLException ex) {
            return null;
        }
    }

   public  void close(){
    } 

}
