/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package instrumento.logica;

import instrumento.datos.Dao;
import instrumento.entidades.Calibracion;
import instrumento.entidades.Instrumento;
import instrumento.entidades.Medida;
import instrumento.entidades.TipoInstrumento;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Steven Villalobos
 */
public class Model {

    private Dao dao;
    private static Model uniqueInstance;
    public static Model instance()
    {
        if (uniqueInstance == null)
        {
            uniqueInstance = new Model();
        }
        return uniqueInstance;
    }
     private Model(){
        dao = new Dao();
    }            
    public Collection<TipoInstrumento> getTipoInstrumentos(){
        return dao.TipoInstrumentoGetAll();
    }
    public List<Calibracion> getCalibracionAll(){
        return dao.CalibracionGetAll();
    }
    public List<Calibracion> getCalibracion(String serie) throws Exception{
        return dao.CalibracionGet(serie);
    }
    public List<Medida> getMedida(int calibracion) throws Exception{
        return dao.MedidaGet(calibracion);
    }
    public  Instrumento getInstrumento(String codigo) throws Exception{
        return dao.InstrumentoGet(codigo);
    }
    public void deleteInstrumento(Instrumento instrumento) throws Exception{
        dao.InstrumentoDelete(instrumento);
    }
    public void deleteTipoInstrumento(TipoInstrumento tipoInstrumento) throws Exception{
        dao.TipoInstrumentoDelete(tipoInstrumento);
    }
    public void deleteCalibracion(Calibracion calibracion) throws Exception{
        dao.CalibracionDelete(calibracion);
    }
   public void addTipoInstrumento(TipoInstrumento tipoInstrumento) throws Exception{
        dao.TipoInstrumentoAdd(tipoInstrumento);
    }
    public void addInstrumento(Instrumento instrumento) throws Exception{
        dao.InstrumentoAdd(instrumento);
    }
    public void addCalibracion(Calibracion calibracion) throws Exception{
        dao.CalibracionAdd(calibracion);
    }
    public void updateInstrumento(Instrumento instrumento) throws Exception{
        dao.InstrumentoUpdate(instrumento);
    }
    public void updateTipoInstrumento(TipoInstrumento tipoInstrumento) throws Exception{
        dao.TipoInstrumentoUpdate(tipoInstrumento);
    } 
    public void updateCalibracion(Calibracion calibracion) throws Exception{
        dao.CalibracionUpdate(calibracion);
    }
    public List<Instrumento> searchInstrumento(Instrumento filtro){
        return dao.InstrumentoSearch(filtro);
    }
    public List<TipoInstrumento> searchTipoInstrumento(TipoInstrumento filtro){
        return dao.TipoInstrumentoSearch(filtro);
    }
    public List<Calibracion> searchCalibracion(Calibracion filtro){
        return dao.CalibracionSearch(filtro);
    }
    public List<Medida> searchMedidas(Calibracion filtro){
        return dao.MedidasSearch(filtro);
    }
    public void close(){
        dao.close();
    }
}
